<?php

namespace Chilemedios\CotizadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attr = array('class' => 'form-control');
        
        $builder
            ->add('nombre', 'text', array(
                'required' => true,
                'attr' => $attr
            ))
            ->add('detalle', 'textarea', array(
                'attr' => $attr
            ))
            ->add('valor', null, array(
                'attr' => $attr
            ))
            ->add('indicador', 'entity', array(
                'class' => 'ChilemediosCotizadorBundle:IndicadorEconomico',
                'property' => 'nombre',
                'attr' => $attr
            ))
            ->add('activo', 'checkbox', array(
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chilemedios\CotizadorBundle\Entity\Producto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chilemedios_cotizadorbundle_producto';
    }
}
