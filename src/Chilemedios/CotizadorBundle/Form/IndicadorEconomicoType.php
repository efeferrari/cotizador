<?php

namespace Chilemedios\CotizadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IndicadorEconomicoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('valor')
            ->add('recargo')
            ->add('simbolo')
            ->add('abreviacion')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chilemedios\CotizadorBundle\Entity\IndicadorEconomico'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chilemedios_cotizadorbundle_indicadoreconomico';
    }
}
