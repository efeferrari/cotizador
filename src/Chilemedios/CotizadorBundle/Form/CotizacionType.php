<?php

namespace Chilemedios\CotizadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CotizacionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attr = array();
        
        $builder
            ->add('dominio', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('rut', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => array(
                    'class' => 'rut form-control',
                    'maxlength' => '13'
                )
            ))
            ->add('razonSocial', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('giro', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('direccion', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('comuna', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('fono', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('mail', 'email', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('contactoTecnicoNombre', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('contactoTecnicoMail', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('contactoTecnicoFono', 'text', array(
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('comentario', 'textarea', array(
                'required' => false,
                'error_bubbling' => true,
                'attr' => $attr
            ))
            ->add('dolar', 'hidden', array(
                'required' => true,
                'error_bubbling' => true,
            ))
            ->add('status', 'entity', array(
                'class' => 'ChilemediosCotizadorBundle:CotizacionStatus',
                'property' => 'nombre',
                'required' => true,
                'error_bubbling' => true,
                'attr' => $attr
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chilemedios\CotizadorBundle\Entity\Cotizacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chilemedios_cotizadorbundle_cotizacion';
    }
}
