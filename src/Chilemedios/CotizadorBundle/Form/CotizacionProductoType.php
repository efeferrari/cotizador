<?php

namespace Chilemedios\CotizadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CotizacionProductoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('valorUnitario')
            ->add('cantidad')
            ->add('productos', 'entity', array(
                'class' => 'ChilemediosCotizadorBundle:Producto',
                'property' => 'nombre'
            ))
            ->add('cotizaciones', 'entity', array(
                'class' => 'ChilemediosCotizadorBundle:Cotizacion',
                'property' => 'id'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chilemedios\CotizadorBundle\Entity\CotizacionProducto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chilemedios_cotizadorbundle_cotizacionproducto';
    }
}
