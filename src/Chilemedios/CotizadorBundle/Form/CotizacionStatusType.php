<?php

namespace Chilemedios\CotizadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CotizacionStatusType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'entity', array(
                'class' => 'ChilemediosCotizadorBundle:CotizacionStatus',
                'property' => 'nombre',
                'empty_value' => 'Todos',
                'required'=> false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chilemedios\CotizadorBundle\Entity\CotizacionStatus'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chilemedios_cotizadorbundle_cotizacionstatus';
    }
}
