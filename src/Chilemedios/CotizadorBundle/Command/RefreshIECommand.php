<?php

namespace Chilemedios\CotizadorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshIECommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('chilemedios:refresh-ie')
                ->setDescription('Actualiza Indicadores Economicos')
                #->addArgument('name', InputArgument::OPTIONAL, 'Who do you want to greet?')
                #->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('refresh_ie')->f5();
    }
    
}
