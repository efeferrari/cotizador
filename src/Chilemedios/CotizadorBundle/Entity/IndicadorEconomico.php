<?php

namespace Chilemedios\CotizadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IndicadorEconomico
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Chilemedios\CotizadorBundle\Entity\IndicadorEconomicoRepository")
 */
class IndicadorEconomico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=8, scale=2)
     */
    private $valor;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="recargo", type="integer", nullable=true)
     */
    private $recargo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="simbolo", type="string", length=2)
     */
    private $simbolo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="abreviacion", type="string", length=10)
     */
    private $abreviacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_updated", type="datetime")
     */
    private $lastUpdated;
    
    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="indicador")
     **/
    private $productos;

    public function __construct()
    {
        $this->lastUpdated = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return IndicadorEconomico
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return IndicadorEconomico
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     * @return IndicadorEconomico
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime 
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }
    
    /**
     * Devuelve el valor del indicador con el recargo de ChileMedios 
     */
    public function getValorCM(){
        return $this->getValor() + $this->getRecargo();
    }

    /**
     * Set recargo
     *
     * @param integer $recargo
     * @return IndicadorEconomico
     */
    public function setRecargo($recargo)
    {
        $this->recargo = $recargo;

        return $this;
    }

    /**
     * Get recargo
     *
     * @return integer 
     */
    public function getRecargo()
    {
        return $this->recargo;
    }

    /**
     * Set simbolo
     *
     * @param string $simbolo
     * @return IndicadorEconomico
     */
    public function setSimbolo($simbolo)
    {
        $this->simbolo = $simbolo;

        return $this;
    }

    /**
     * Get simbolo
     *
     * @return string 
     */
    public function getSimbolo()
    {
        return $this->simbolo;
    }

    /**
     * Set abreviacion
     *
     * @param string $abreviacion
     * @return IndicadorEconomico
     */
    public function setAbreviacion($abreviacion)
    {
        $this->abreviacion = $abreviacion;

        return $this;
    }

    /**
     * Get abreviacion
     *
     * @return string 
     */
    public function getAbreviacion()
    {
        return $this->abreviacion;
    }

    /**
     * Add productos
     *
     * @param \Chilemedios\CotizadorBundle\Entity\Producto $productos
     * @return IndicadorEconomico
     */
    public function addProducto(\Chilemedios\CotizadorBundle\Entity\Producto $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Chilemedios\CotizadorBundle\Entity\Producto $productos
     */
    public function removeProducto(\Chilemedios\CotizadorBundle\Entity\Producto $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
