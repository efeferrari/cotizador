<?php

namespace Chilemedios\CotizadorBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CotizacionProductoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CotizacionProductoRepository extends EntityRepository
{
}
