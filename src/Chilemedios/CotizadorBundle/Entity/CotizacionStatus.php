<?php

namespace Chilemedios\CotizadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CotizacionStatus
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CotizacionStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;
    
    /**
     * @ORM\OneToMany(targetEntity="Cotizacion", mappedBy="status")
     **/
    private $cotizaciones;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CotizacionStatus
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cotizaciones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cotizaciones
     *
     * @param \Chilemedios\CotizadorBundle\Entity\Cotizacion $cotizaciones
     * @return CotizacionStatus
     */
    public function addCotizacione(\Chilemedios\CotizadorBundle\Entity\Cotizacion $cotizaciones)
    {
        $this->cotizaciones[] = $cotizaciones;

        return $this;
    }

    /**
     * Remove cotizaciones
     *
     * @param \Chilemedios\CotizadorBundle\Entity\Cotizacion $cotizaciones
     */
    public function removeCotizacione(\Chilemedios\CotizadorBundle\Entity\Cotizacion $cotizaciones)
    {
        $this->cotizaciones->removeElement($cotizaciones);
    }

    /**
     * Get cotizaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCotizaciones()
    {
        return $this->cotizaciones;
    }
}
