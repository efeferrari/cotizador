<?php

namespace Chilemedios\CotizadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CotizacionProducto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Chilemedios\CotizadorBundle\Entity\CotizacionProductoRepository")
 */
class CotizacionProducto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cotizacionId", type="integer")
     */
    private $cotizacionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="productoId", type="integer")
     */
    private $productoId;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_unitario", type="decimal", precision=5, scale=2)
     */
    private $valorUnitario;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;
    
    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="cotizacionProducto")
     * @ORM\JoinColumn(name="productoId", referencedColumnName="id")
     **/
    private $productos;
    
    /**
     * @ORM\ManyToOne(targetEntity="Cotizacion", inversedBy="cotizacionProducto")
     * @ORM\JoinColumn(name="cotizacionId", referencedColumnName="id")
     **/
    private $cotizaciones;

    public function __construct()
    {
        $this->productos    = new ArrayCollection();
        $this->cotizaciones = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cotizacionId
     *
     * @param integer $cotizacionId
     * @return CotizacionProducto
     */
    public function setCotizacionId($cotizacionId)
    {
        $this->cotizacionId = $cotizacionId;

        return $this;
    }

    /**
     * Get cotizacionId
     *
     * @return integer 
     */
    public function getCotizacionId()
    {
        return $this->cotizacionId;
    }

    /**
     * Set productoId
     *
     * @param integer $productoId
     * @return CotizacionProducto
     */
    public function setProductoId($productoId)
    {
        $this->productoId = $productoId;

        return $this;
    }

    /**
     * Get productoId
     *
     * @return integer 
     */
    public function getProductoId()
    {
        return $this->productoId;
    }

    /**
     * Set valorUnitario
     *
     * @param string $valorUnitario
     * @return CotizacionProducto
     */
    public function setValorUnitario($valorUnitario)
    {
        $this->valorUnitario = $valorUnitario;

        return $this;
    }

    /**
     * Get valorUnitario
     *
     * @return string 
     */
    public function getValorUnitario()
    {
        return $this->valorUnitario;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return CotizacionProducto
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set producto
     *
     * @param \Chilemedios\CotizadorBundle\Entity\Producto $productos
     * @return CotizacionProducto
     */
    public function setProductos(\Chilemedios\CotizadorBundle\Entity\Producto $productos = null)
    {
        $this->productos = $productos;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \Chilemedios\CotizadorBundle\Entity\Producto 
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Set cotizacion
     *
     * @param \Chilemedios\CotizadorBundle\Entity\Cotizacion $cotizacion
     * @return CotizacionProducto
     */
    public function setCotizaciones(\Chilemedios\CotizadorBundle\Entity\Cotizacion $cotizaciones = null)
    {
        $this->cotizaciones = $cotizaciones;

        return $this;
    }

    /**
     * Get cotizacion
     *
     * @return \Chilemedios\CotizadorBundle\Entity\Cotizacion 
     */
    public function getCotizaciones()
    {
        return $this->cotizaciones;
    }
}
