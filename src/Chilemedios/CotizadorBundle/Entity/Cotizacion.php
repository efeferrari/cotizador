<?php

namespace Chilemedios\CotizadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cotizacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Chilemedios\CotizadorBundle\Entity\CotizacionRepository")
 */
class Cotizacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dominio", type="string", length=255)
     */
    private $dominio;

    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", length=12)
     */
    private $rut;
    
    /**
     * @var string
     *
     * @ORM\Column(name="razonSocial", type="string", length=255)
     */
    private $razonSocial;
    
    /**
     * @var string
     *
     * @ORM\Column(name="giro", type="string", length=255)
     */
    private $giro;
    
    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comuna", type="string", length=255)
     */
    private $comuna;
    
    /**
     * @var string
     *
     * @ORM\Column(name="fono", type="string", length=255)
     */
    private $fono;
    
    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contactoTecnicoNombre", type="string", length=255)
     */
    private $contactoTecnicoNombre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contactoTecnicoMail", type="string", length=255)
     */
    private $contactoTecnicoMail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contactoTecnicoFono", type="string", length=255)
     */
    private $contactoTecnicoFono;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=255, nullable=true)
     */
    private $comentario;
    
    /**
     * @var decimal Guarda el valor del dolar al momento de generar la cotización
     *
     * @ORM\Column(name="dolar", type="decimal", precision=5, scale=2)
     */
    private $dolar;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="statusId", type="integer")
     */
    private $statusId;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\OneToMany(targetEntity="CotizacionProducto", mappedBy="cotizaciones", cascade={"remove"})
     **/
    private $cotizacionProducto;
    
    /**
     * @ORM\ManyToOne(targetEntity="CotizacionStatus", inversedBy="cotizaciones")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id")
     **/
    private $status;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->cotizacionProducto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return Cotizacion
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return Cotizacion
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set giro
     *
     * @param string $giro
     * @return Cotizacion
     */
    public function setGiro($giro)
    {
        $this->giro = $giro;

        return $this;
    }

    /**
     * Get giro
     *
     * @return string 
     */
    public function getGiro()
    {
        return $this->giro;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Cotizacion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set comuna
     *
     * @param string $comuna
     * @return Cotizacion
     */
    public function setComuna($comuna)
    {
        $this->comuna = $comuna;

        return $this;
    }

    /**
     * Get comuna
     *
     * @return string 
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Set fono
     *
     * @param string $fono
     * @return Cotizacion
     */
    public function setFono($fono)
    {
        $this->fono = $fono;

        return $this;
    }

    /**
     * Get fono
     *
     * @return string 
     */
    public function getFono()
    {
        return $this->fono;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Cotizacion
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set contactoTecnicoNombre
     *
     * @param string $contactoTecnicoNombre
     * @return Cotizacion
     */
    public function setContactoTecnicoNombre($contactoTecnicoNombre)
    {
        $this->contactoTecnicoNombre = $contactoTecnicoNombre;

        return $this;
    }

    /**
     * Get contactoTecnicoNombre
     *
     * @return string 
     */
    public function getContactoTecnicoNombre()
    {
        return $this->contactoTecnicoNombre;
    }

    /**
     * Set contactoTecnicoMail
     *
     * @param string $contactoTecnicoMail
     * @return Cotizacion
     */
    public function setContactoTecnicoMail($contactoTecnicoMail)
    {
        $this->contactoTecnicoMail = $contactoTecnicoMail;

        return $this;
    }

    /**
     * Get contactoTecnicoMail
     *
     * @return string 
     */
    public function getContactoTecnicoMail()
    {
        return $this->contactoTecnicoMail;
    }

    /**
     * Set contactoTecnicoFono
     *
     * @param string $contactoTecnicoFono
     * @return Cotizacion
     */
    public function setContactoTecnicoFono($contactoTecnicoFono)
    {
        $this->contactoTecnicoFono = $contactoTecnicoFono;

        return $this;
    }

    /**
     * Get contactoTecnicoFono
     *
     * @return string 
     */
    public function getContactoTecnicoFono()
    {
        return $this->contactoTecnicoFono;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return Cotizacion
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set dolar
     *
     * @param string $dolar
     * @return Cotizacion
     */
    public function setDolar($dolar)
    {
        $this->dolar = $dolar;

        return $this;
    }

    /**
     * Get dolar
     *
     * @return string 
     */
    public function getDolar()
    {
        return $this->dolar;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     * @return Cotizacion
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer 
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Cotizacion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add cotizacionProducto
     *
     * @param \Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto
     * @return Cotizacion
     */
    public function addCotizacionProducto(\Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto)
    {
        $this->cotizacionProducto[] = $cotizacionProducto;

        return $this;
    }

    /**
     * Remove cotizacionProducto
     *
     * @param \Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto
     */
    public function removeCotizacionProducto(\Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto)
    {
        $this->cotizacionProducto->removeElement($cotizacionProducto);
    }

    /**
     * Get cotizacionProducto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCotizacionProducto()
    {
        return $this->cotizacionProducto;
    }

    /**
     * Set status
     *
     * @param \Chilemedios\CotizadorBundle\Entity\CotizacionStatus $status
     * @return Cotizacion
     */
    public function setStatus(\Chilemedios\CotizadorBundle\Entity\CotizacionStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Chilemedios\CotizadorBundle\Entity\CotizacionStatus 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dominio
     *
     * @param string $dominio
     * @return Cotizacion
     */
    public function setDominio($dominio)
    {
        $this->dominio = $dominio;

        return $this;
    }

    /**
     * Get dominio
     *
     * @return string 
     */
    public function getDominio()
    {
        return $this->dominio;
    }
}
