<?php

namespace Chilemedios\CotizadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Producto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Chilemedios\CotizadorBundle\Entity\ProductoRepository")
 */
class Producto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="detalle", type="string", length=255)
     */
    private $detalle;

    /**
     * @var integer
     *
     * @ORM\Column(name="valor", type="decimal", precision=5, scale=2)
     */
    private $valor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="indicadorId", type="integer")
     */
    private $indicadorId;

    /**
     * @ORM\OneToMany(targetEntity="CotizacionProducto", mappedBy="producto")
     **/
    private $cotizacionProducto;
    
    /**
     * @ORM\ManyToOne(targetEntity="IndicadorEconomico", inversedBy="productos")
     * @ORM\JoinColumn(name="indicadorId", referencedColumnName="id")
     **/
    private $indicador;

    public function __construct()
    {
        $this->cotizacionProducto = new ArrayCollection();
        $this->tipoindicador = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Producto
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     * @return Producto
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return integer 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Producto
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }


    /**
     * Add cotizacionProducto
     *
     * @param \Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto
     * @return Producto
     */
    public function addCotizacionProducto(\Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto)
    {
        $this->cotizacionProducto[] = $cotizacionProducto;

        return $this;
    }

    /**
     * Remove cotizacionProducto
     *
     * @param \Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto
     */
    public function removeCotizacionProducto(\Chilemedios\CotizadorBundle\Entity\CotizacionProducto $cotizacionProducto)
    {
        $this->cotizacionProducto->removeElement($cotizacionProducto);
    }

    /**
     * Get cotizacionProducto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCotizacionProducto()
    {
        return $this->cotizacionProducto;
    }


    /**
     * Set indicadorId
     *
     * @param integer $indicadorId
     * @return Producto
     */
    public function setIndicadorId($indicadorId)
    {
        $this->indicadorId = $indicadorId;

        return $this;
    }

    /**
     * Get indicadorId
     *
     * @return integer 
     */
    public function getIndicadorId()
    {
        return $this->indicadorId;
    }

    /**
     * Set indicador
     *
     * @param \Chilemedios\CotizadorBundle\Entity\IndicadorEconomico $indicador
     * @return Producto
     */
    public function setIndicador(\Chilemedios\CotizadorBundle\Entity\IndicadorEconomico $indicador = null)
    {
        $this->indicador = $indicador;

        return $this;
    }

    /**
     * Get indicador
     *
     * @return \Chilemedios\CotizadorBundle\Entity\IndicadorEconomico 
     */
    public function getIndicador()
    {
        return $this->indicador;
    }
}
