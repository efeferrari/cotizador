$( document ).ready(function() {

    dolarcm = $('#usdcm').attr('usdcm');

    $('.agrega').on('click', function(e)
    {
        // Evitar el # en la url
        e.preventDefault();

        // Ocultar el TR de tabla productos
        ocultaTrTablaProd(this.id);

        // Agrega TR a la cotización
        objProd = $('#'+this.id);
        agregaProdCotizacion(this.id, objProd.attr('nombre'), objProd.attr('vu'), objProd.attr('label'), objProd.attr('ie'));

        // Actualizar Total Neto, I.V.A. y Total
        refrescaTotales();

        // Toggle facilidad de pago
        toggleTrCheques();

    });
    
    $('#cotizacion_form').on('submit', function(e){
        
        e.preventDefault(); // Nunca submit

        productos = [];

        // Revisar la tabla con los productos agregados
        $('#tabla_cotizacion tbody tr.tr-cot-prod').each(function(index, value){
            
            idProdCotizado = $(value).attr('id').split('cot');
            
            idProdCotizado = idProdCotizado[1];
            
            objDataProd = $('#dataProd'+ idProdCotizado);
            
            cantProdCot    = $('#cant'+ idProdCotizado).val();
            
            vuProdCot      = objDataProd.attr('vu');
            
            ieProdCot      = objDataProd.attr('ie');
            
            prod = [ idProdCotizado, cantProdCot , vuProdCot , ieProdCot];
            
            productos.push( prod );
            
            
        });
        
        
        $.ajax({
            url         :   this.action,
            data        :   $('#cotizacion_form').serialize() + '&prods=' + JSON.stringify(productos),
            cache       :   false,
            method      :   'post',
            dataType    :   'json',
            beforeSend  :   function(){
                                bloquearSubmit();
            },
            success     :   function(reply){
                                
                                if( reply.toString() == 'true')
                                {
                                    // Mostrar mensaje de éxito
                                    mostrarSuccess();
                                    // Ocultar todo
                                    irPasoTres();
                                }
                                else
                                {
                                    // Mostrar mensaje de error
                                    mostrarErrores(reply);
                                    
                                }
            },
            complete  :   function(){
                                liberarSubmit();
            },
            statusCode  :   {
                404     :   function(){
                    alert('Página no encontrada');
                },
                500     :   function(){
                    alert('Error del servidor');
                },
            }
            
        });
        
    });
    
    /********************************************************** Validador rut */
    $(".rut").Rut({
        format_on: 'keyup',
        on_error: function(){
            $('.Rut').attr('style', 'border: 1px solid #a94442');
            alert('El rut no es válido');
            bloquearSubmit(true); // envio true para no mostrar logo loading
        },
        on_success: function(){
            $('.Rut').removeAttr('style');
            liberarSubmit();
        },
    });
    
});

/********************************************************** Tabla Cotización **/

function agregaProdCotizacion(pid, nombre, vu, label, ie)
{
    totalNetoProd = totalNetoProducto(vu, 1, ie);

    $("#tr_total_neto").before('\
        <tr style="display:none;" id="cot'+ pid +'" class="tr-cot-prod">\
            <td class="center">\
                <button class="btn btn-bricky" onclick="eliminaProdCotizacion('+ pid +');"><i class="fa fa-trash-o"></i></button>\
            </td>\
            <td>'+ nombre +'</td>\
            <td>'+ label +'&nbsp;'+ vu +'</td>\
            <td>\
                <input id="cant'+ pid +'" onkeyup="refrescaCotizacion('+ pid +')" onchange="refrescaCotizacion('+ pid +')" type="number" value="1">\
            </td>\
            <td class="total">\
                $&nbsp;<span id="total'+ pid +'">'+ formateaNumero(totalNetoProd) +'</span>\
                <span id="dataProd'+ pid +'" vu="'+ vu +'" ie="'+ ie +'"></span>\
                <span id="totalNetoProd'+ pid +'" class="totalNetoProd" style="display:none;">'+ totalNetoProd +'</span>\
            </td>\
        </tr>\
    ');
    
    $('#cot'+pid).show(500);

}

/**
 *  Elimina producto de la cotización, lo vuelva a agregar al listado de
 * productos disponibles y actualiza los totales
 *
 */
function eliminaProdCotizacion(pid)
{
    $('#cot'+ pid).hide(300).remove();

    muestraTrTablaProd(pid);
    
    refrescaTotales();
    toggleTrCheques();

}

function refrescaCotizacion(pid)
{
    // Objeto del Span con los datos del producto que le cambiaron la cantidad
    objSpanData = $('#dataProd'+ pid);
    ie   = objSpanData.attr('ie');
    vu   = objSpanData.attr('vu');
    cant = $('#cant'+ pid).val();
    
    if(cant < 1 || (cant*1) === NaN)
    {
        cant = 1;
        $('#cant'+ pid).val(cant);
    }
    
    // Total Neto del Producto
    totalProd = totalNetoProducto(vu, cant, ie);
    
    // Valor formateado para mostrar al usuario
    $('#total'+ pid).html(formateaNumero(totalProd));
    // Valor sin formato para hacer operaciones
    $('#totalNetoProd'+ pid).html( totalProd );
    
    // Actualizar Total Neto, I.V.A. y Total
    refrescaTotales();
    
    // Atualizar valor cheque
    toggleTrCheques();

}

function refrescaTotales()
{
    // Calcular la suma total de los Valores Neto
    totalNeto = calcularTotalNeto();
    
    // Calcular el I.V.A. general
    iva = calcularIVA(totalNeto);
    
    // Calcular Total de la cotización
    totalCotizacion = calcularTotal(totalNeto, iva);
    
    // Calcular el pago en tres cheques.
    cheques = calcularPagoCheque(totalCotizacion);
    
    // Mostrar los valores
    $('#total_neto').html(formateaNumero(totalNeto));
    $('#total_iva').html(formateaNumero(iva));
    $('#total_cotizacion').html(formateaNumero(totalCotizacion));
}

/**
 * Calcula el monto de los cheques y muestra/oculta el mensaje
 *
 */
function toggleTrCheques()
{
    totalCot = limpiaNumero($('#total_cotizacion').html());
    
    if(totalCot > 0)
    {
        cheque = Math.round(totalCot / 3);
        $('#tr-cheque').show(500);
    }
    else
    {
        cheque = 0;
        $('#tr-cheque').hide(500);
    }
    
    $('#cheques').html( formateaNumero(cheque) );
    
}

/**
 * Calcula el Total Neto de la cotización
 *
 */
function calcularTotalNeto()
{
    totalCot = 0;
    
    $('.totalNetoProd').each(function(index, value){
        totalCot += ( $(value).html() )*1;
    });
    
    return totalCot;
}

/**
 * Calcula el I.V.A. de la cotización
 *
 */
function calcularIVA(neto)
{
    return neto * 0.19
}

/**
 * Calcula el Total de la cotización
 *
 */
function calcularTotal(neto, iva)
{
    return neto + iva;
}

/**
 * Calcula el Total Neto de un Producto
 *
 */
function totalNetoProducto(vu, cant, indicadorEconomico)
{
    return Math.round( (vu * indicadorEconomico * cant ) );
}

/**
 * Calcula el monto de cada cheque
 *
 */
function calcularPagoCheque(n)
{
    return n/3;
}

/**
 * Agrega los puntos de miles a un número y define la cantidad de decimales
 *
 */
function formateaNumero(num, dec)
{
    if (!dec) dec = 0;
    return $.number(num, dec, ',', '.');
}

/**
 * Saca puntos y cambia las comas por puntos
 *
 */
function limpiaNumero(num)
{
    sinPuntos = num.replace(/\./g, '');
    comasAPuntos = sinPuntos.replace(/\,/g, '.');
    return comasAPuntos * 1;
}

/***************************************************** Bloquear boton submit **/

function bloquearSubmit(loading)
{
    $('#chilemedios_cotizadorbundle_cotizacion_submit').attr('disabled', 'disabled');
    $('#chilemedios_cotizadorbundle_cotizacion_submit').addClass('disabled');
    
    if (!loading)
    {
        $('#ajaxLoader').show(100);
        $('#chilemedios_cotizadorbundle_cotizacion_submit').html('Enviando cotización...');
    }
}

function liberarSubmit()
{
    $('#chilemedios_cotizadorbundle_cotizacion_submit').removeAttr('disabled');
    $('#chilemedios_cotizadorbundle_cotizacion_submit').removeClass('disabled');
    $('#chilemedios_cotizadorbundle_cotizacion_submit').html('Enviar cotizacion');
    $('#ajaxLoader').hide(100);
    
    // Cambiar captcha después de liberar, en caso que se encontraran errores
    reload_captcha();
}

/*********************************************** Tabla Productos disponibles **/

function ocultaTrTablaProd(value)
{
    $('#tr-'+ value).hide(500);
}

function muestraTrTablaProd(value)
{
    $('#tr-'+ value).show(500);
}

/***************************************************************** Pantallas **/

function irPasoDos(){
    $('#cot_page1').hide(500);
    $('#cot_page2').show(500);
}

function irPasoUno(){
    $('#cot_page2').hide(500);
    $('#cot_page1').show(500);
}

function irPasoTres(){
    $('#cot_page1').hide();
    $('#cot_page2').hide(500);
    $('#cot_page3').show(500);
}


function mostrarSuccess(){
    $('#mensajeSuccess').show(100);
}

// Muestra mensaje de error en submit
function mostrarError(errores){
    $('#mensajeError').show(100);
}

function mostrarErrores(arr)
{
    var text = '';
    $.each(arr, function(index, value){
        text += '<li>'+ value +'</li>';
    });
    
    $('#lista_errores').html(text);
    $('#globo_errores').show(200);
}









