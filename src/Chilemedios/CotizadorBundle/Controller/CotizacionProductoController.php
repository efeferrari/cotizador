<?php

namespace Chilemedios\CotizadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Chilemedios\CotizadorBundle\Entity\CotizacionProducto;
use Chilemedios\CotizadorBundle\Form\CotizacionProductoType;

/**
 * CotizacionProducto controller.
 *
 * @Route("/cotizacionproducto")
 */
class CotizacionProductoController extends Controller
{

    /**
     * Lists all CotizacionProducto entities.
     *
     * @Route("/", name="cotizacionproducto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ChilemediosCotizadorBundle:CotizacionProducto')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new CotizacionProducto entity.
     *
     * @Route("/", name="cotizacionproducto_create")
     * @Method("POST")
     * @Template("ChilemediosCotizadorBundle:CotizacionProducto:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CotizacionProducto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cotizacionproducto_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a CotizacionProducto entity.
    *
    * @param CotizacionProducto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(CotizacionProducto $entity)
    {
        $form = $this->createForm(new CotizacionProductoType(), $entity, array(
            'action' => $this->generateUrl('cotizacionproducto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CotizacionProducto entity.
     *
     * @Route("/new", name="cotizacionproducto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new CotizacionProducto();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a CotizacionProducto entity.
     *
     * @Route("/{id}", name="cotizacionproducto_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:CotizacionProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CotizacionProducto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CotizacionProducto entity.
     *
     * @Route("/{id}/edit", name="cotizacionproducto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:CotizacionProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CotizacionProducto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CotizacionProducto entity.
    *
    * @param CotizacionProducto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CotizacionProducto $entity)
    {
        $form = $this->createForm(new CotizacionProductoType(), $entity, array(
            'action' => $this->generateUrl('cotizacionproducto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CotizacionProducto entity.
     *
     * @Route("/{id}", name="cotizacionproducto_update")
     * @Method("PUT")
     * @Template("ChilemediosCotizadorBundle:CotizacionProducto:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:CotizacionProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CotizacionProducto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cotizacionproducto_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CotizacionProducto entity.
     *
     * @Route("/{id}", name="cotizacionproducto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ChilemediosCotizadorBundle:CotizacionProducto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CotizacionProducto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cotizacionproducto'));
    }

    /**
     * Creates a form to delete a CotizacionProducto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cotizacionproducto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
