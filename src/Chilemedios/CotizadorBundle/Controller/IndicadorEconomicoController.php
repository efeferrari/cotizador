<?php

namespace Chilemedios\CotizadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Chilemedios\CotizadorBundle\Entity\IndicadorEconomico;
use Chilemedios\CotizadorBundle\Form\IndicadorEconomicoType;

/**
 * IndicadorEconomico controller.
 *
 * @Route("/indicadoreconomico")
 */
class IndicadorEconomicoController extends Controller
{

    /**
     * Lists all IndicadorEconomico entities.
     *
     * @Route("/", name="indicadoreconomico")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new IndicadorEconomico entity.
     *
     * @Route("/", name="indicadoreconomico_create")
     * @Method("POST")
     * @Template("ChilemediosCotizadorBundle:IndicadorEconomico:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new IndicadorEconomico();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $this->get('flash_messages')->nuevo('notice', 'Indicador económico creado');

            return $this->redirect($this->generateUrl('indicadoreconomico_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a IndicadorEconomico entity.
    *
    * @param IndicadorEconomico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(IndicadorEconomico $entity)
    {
        $form = $this->createForm(new IndicadorEconomicoType(), $entity, array(
            'action' => $this->generateUrl('indicadoreconomico_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Nuevo', 'attr'=> array('class' => 'btn btn-primary btn-fix')));

        return $form;
    }

    /**
     * Displays a form to create a new IndicadorEconomico entity.
     *
     * @Route("/new", name="indicadoreconomico_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new IndicadorEconomico();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a IndicadorEconomico entity.
     *
     * @Route("/{id}", name="indicadoreconomico_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IndicadorEconomico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing IndicadorEconomico entity.
     *
     * @Route("/{id}/edit", name="indicadoreconomico_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IndicadorEconomico entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a IndicadorEconomico entity.
    *
    * @param IndicadorEconomico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(IndicadorEconomico $entity)
    {
        $form = $this->createForm(new IndicadorEconomicoType(), $entity, array(
            'action' => $this->generateUrl('indicadoreconomico_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar', 'attr'=> array('class' => 'btn btn-success btn-fix')));

        return $form;
    }
    /**
     * Edits an existing IndicadorEconomico entity.
     *
     * @Route("/{id}", name="indicadoreconomico_update")
     * @Method("PUT")
     * @Template("ChilemediosCotizadorBundle:IndicadorEconomico:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IndicadorEconomico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('flash_messages')->nuevo('notice', 'Indicador económico actualizado');
            
            return $this->redirect($this->generateUrl('indicadoreconomico_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a IndicadorEconomico entity.
     *
     * @Route("/{id}", name="indicadoreconomico_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find IndicadorEconomico entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('flash_messages')->nuevo('notice', 'Indicador económico eliminado');
        }

        return $this->redirect($this->generateUrl('indicadoreconomico'));
    }

    /**
     * Creates a form to delete a IndicadorEconomico entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('indicadoreconomico_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar', 'attr' => array('class' => 'btn btn-danger btn-fix')))
            ->getForm()
        ;
    }
    
    /**
     * Actualización "manual" de los indicadores económicos
     * 
     * @Route("/ie_update/", name="ie_update")
     */
    public function actualizaIndicadoresAction()
    {
        
        if($this->get('refresh_ie')->f5() === true)
        {
            $this->get('flash_messages')->nuevo('notice', 'Los Indicadores Económicos fueron actualizados!');
        }
        else
        {
            $this->get('flash_messages')->nuevo('notice', 'Hubo un error al actualizar los Indicadores Económicos!');

        }
        
        return $this->redirect($this->generateUrl('indicadoreconomico'));
        
    }
    
}
