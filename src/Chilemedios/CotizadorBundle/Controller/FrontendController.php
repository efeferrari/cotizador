<?php

namespace Chilemedios\CotizadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Chilemedios\CotizadorBundle\Entity\Cotizacion;
use Chilemedios\CotizadorBundle\Entity\CotizacionProducto;
use Chilemedios\CotizadorBundle\Form\CotizacionType;

use Ps\PdfBundle\Annotation\Pdf;

class FrontendController extends Controller
{
    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $productos = $em->getRepository('ChilemediosCotizadorBundle:Producto')->getActivos();
        
        $dolarcm = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find(1);
        
        $entity = new Cotizacion();
        $form = $this->createCreateForm($entity);
        
        return array(
            'productos' => $productos,
            'dolarcm'   => ($dolarcm->getValor() + $dolarcm->getRecargo()),
            'form'      => $form->createView()
        );
    }
    
    /**
     * @Route("/nuevaCotizacion", name="cotizacion_nueva")
     * @Template()
     */
    public function nuevaCotizacionAction(Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $entity = new Cotizacion();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);
            
            /*
            echo "<pre>";
            print_r($_POST);
            echo "</pre>";
            die;
            */
            
            if ($form->isValid())
            {
                try
                {
                    $em = $this->getDoctrine()->getManager();
                    
                    # Obtener valor del dolar en la BD
                    $dolar = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find(1);
                    
                    # Guardar el dolar en la cotización
                    $entity->setDolar($dolar->getValor() + $dolar->getRecargo() );
                    $entity->setStatus($em->getRepository('ChilemediosCotizadorBundle:CotizacionStatus')->find(1)); # Estado Pendiente
                    
                    $em->persist($entity);
                    
                    $prods = json_decode($request->request->get('prods'));

                    if(get_magic_quotes_gpc())
                    {
                        $prods = json_decode( stripslashes( $request->request->get('prods') ) );
                    }
                    
                    foreach($prods as $p)
                    {
                        $cp = new CotizacionProducto();
                        $cp->setCotizaciones($entity);
                        $cp->setProductos($em->getRepository('ChilemediosCotizadorBundle:Producto')->find($p[0]));
                        $cp->setCantidad($p[1]);
                        $cp->setValorUnitario($p[2]);
                        
                        $em->persist($cp);
                    }
                    
                    $em->flush();
                }
                catch (Exception $e)
                {
                    
                }
                
                $entida = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->getOneFull($entity->getId(), true);

                # Generar PDF con la cotización
                $pdf = $this->generaPdf(
                        $entida
                    );
                
                $this->enviarNotificaciones($entida, $pdf);
                
                return new Response('true');
            }
            else
            {
                
                $errors = array();
                
                foreach ($form->getErrors() as $key => $error) {
                    $errors[] = $error->getMessage();
                }
                
                return new Response(json_encode($errors));
            }
            
        }
        
        throw $this->createNotFoundException('Acceso no válido');
        
    }
    
    private function createCreateForm(Cotizacion $entity)
    {
        $form = $this->createForm(new CotizacionType(), $entity, array(
            'action' => $this->generateUrl('cotizacion_nueva'),
            'method' => 'POST',
        ));

        $form
            ->remove('status')
            ->add('captcha', 'captcha', array(
                'error_bubbling' => true,
            ))
            ->add('submit', 'submit',
                array
                (
                    'label' => "Enviar cotización",
                    'attr'  => array
                    (
                        'class' => 'btn btn-primary btn-fix ladda-button',
                        'data-style' => 'expand-right',
                    )
                )
                )
            ;

        return $form;
    }

    public function enviarNotificaciones(&$cotizacion, $pdf)
    {
        /*
        echo "<pre>";
        print_r($cotizacion);
        echo "</pre>";
        die('jir');
        */
        
        //************************************************************** Cliente
        $message = \Swift_Message::newInstance()
            ->setSubject('[Chilemedios] Cotización de servicios')
            ->setFrom('cotizaciones@chilemedios.cl')
            ->setTo($cotizacion['mail'])
            ->setBody(
                $this->renderView(
                    'ChilemediosCotizadorBundle:Frontend:mailConfirmacionCliente.html.twig',
                    array('cotizacion' => $cotizacion)
                ),
                'text/html'
            )
            ->attach(\Swift_Attachment::fromPath($pdf))
        ;
        
        $this->get('mailer')->send($message);
        
        //**************************************************************** Admin
/*
        $message = \Swift_Message::newInstance()
            ->setSubject('[Chilemedios] Se ha recibido una cotización de servicios')
            ->setFrom('cotizaciones@chilemedios.cl')
            ->setTo('efe.ferrari@gmail.com')
            ->setBody(
                $this->renderView(
                    'ChilemediosCotizadorBundle:Frontend:mailConfirmacionCliente.html.twig',
                    array('cotizacion' => $cotizacion)
                ),
                'text/html'
            )
        ;
        
        $this->get('mailer')->send($message);
*/

    }

    /**
     * @Route("/demo", name="demo")
     * @Template()
     */
    public function demoAction()
    {
        return array();
    }
    
    public function generaPdf($cotizacion)
    {
        
        $nonvre = '/tmp/' . date('Ymdhis') . '.pdf';
        
        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView(
                    'ChilemediosCotizadorBundle:Frontend:cotizacion.html.twig',
                    array(
                        'entity'  => $cotizacion
                    )
                ),
                $nonvre
            );

        return $nonvre;
        
    }

}
