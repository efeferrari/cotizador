<?php

namespace Chilemedios\CotizadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Chilemedios\CotizadorBundle\Entity\Cotizacion;
use Chilemedios\CotizadorBundle\Form\CotizacionType;

# Servicio de Flashes
use Chilemedios\UtilsBundle\FlashMessages;
use Chilemedios\CotizadorBundle\Entity\CotizacionStatus;

/**
 * Cotizacion controller.
 *
 * @Route("/cotizacion")
 */
class CotizacionController extends Controller
{

    /**
     * Lists all Cotizacion entities.
     *
     * @Route("/", name="cotizacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $filterForm = $this->filtroEstadoForm();
        
        #$entities = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->findBy($params);
        $entities = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->getPendientes();

        return array(
            'entities' => $entities,
            'filterForm' => $filterForm->createView()
        );
    }
    
    /**
     * Cotizaciones filtradas.
     *
     * @Route("/filtrado/", name="cotizacion_filtradas")
     * @Method("POST")
     * @Template("ChilemediosCotizadorBundle:Cotizacion:index.html.twig")
     */
    public function filtrarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $filterForm = $this->filtroEstadoForm();
        
        $filterForm->handleRequest($request);
        
        if ($filterForm->isValid())
        {
            $estado = (is_object( $filterForm->getData()->getNombre() ) )? $filterForm->getData()->getNombre()->getId() : null;
        }

        $entities = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->getPendientes($estado);

        return array(
            'entities' => $entities,
            'filterForm' => $filterForm->createView()
        );
    }
    
    /**
     * Creates a new Cotizacion entity.
     *
     * @Route("/", name="cotizacion_create")
     * @Method("POST")
     * @Template("ChilemediosCotizadorBundle:Cotizacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Cotizacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $this->get('flash_messages')->nuevo('notice', 'Cotización creada');

            return $this->redirect($this->generateUrl('cotizacion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Cotizacion entity.
    *
    * @param Cotizacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Cotizacion $entity)
    {
        $form = $this->createForm(new CotizacionType(), $entity, array(
            'action' => $this->generateUrl('cotizacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Nuevo', 'attr'=> array('class' => 'btn btn-primary btn-fix')));

        return $form;
    }

    /**
     * Displays a form to create a new Cotizacion entity.
     *
     * @Route("/new", name="cotizacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Cotizacion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Cotizacion entity.
     *
     * @Route("/{id}", name="cotizacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->getOneFull($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cotizacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Cotizacion entity.
     *
     * @Route("/{id}/edit", name="cotizacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cotizacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Cotizacion entity.
    *
    * @param Cotizacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Cotizacion $entity)
    {
        $form = $this->createForm(new CotizacionType(), $entity, array(
            'action' => $this->generateUrl('cotizacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar', 'attr'=> array('class' => 'btn btn-success btn-fix')));

        return $form;
    }
    /**
     * Edits an existing Cotizacion entity.
     *
     * @Route("/{id}", name="cotizacion_update")
     * @Method("PUT")
     * @Template("ChilemediosCotizadorBundle:Cotizacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cotizacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            
            $this->get('flash_messages')->nuevo('notice', 'Cotización actualizada');

            return $this->redirect($this->generateUrl('cotizacion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Cotizacion entity.
     *
     * @Route("/{id}", name="cotizacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ChilemediosCotizadorBundle:Cotizacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cotizacion entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('flash_messages')->nuevo('notice', 'Cotización eliminada');
        }

        return $this->redirect($this->generateUrl('cotizacion'));
    }

    /**
     * Creates a form to delete a Cotizacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cotizacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar', 'attr' => array('class' => 'btn btn-danger btn-fix')))
            ->getForm()
        ;
    }
    
    public function filtroEstadoForm()
    {
        $obj = new CotizacionStatus();
        return $this->createForm(
                new \Chilemedios\CotizadorBundle\Form\CotizacionStatusType(), $obj, array(
                    'action' => $this->generateUrl('cotizacion_filtradas'),
                    'method' => 'POST',
                )
            )
            ->add('submit', 'submit',
                    array(
                        'label' => 'Filtrar',
                        'attr'  => 
                            array(
                                'class' => 'btn btn-dark-beige btn-fix'
                            )
                    )
                )
        ;
    }
    
}
