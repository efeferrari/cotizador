<?php

namespace Chilemedios\UtilsBundle\FlashMessages;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FlashMessages
{
    private $container;
    
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Crea un mensaje flash
     * @param string $tipo
     * @param string $mensaje 
     */
    public function nuevo($tipo = null, $mensaje = null)
    {
        if(!is_null($tipo) || !is_null($mensaje))
        {
            $this->container->get('session')->getFlashBag()->add(
                $tipo,
                $mensaje
            );
        } else
        {
            throw new \Exception('Mensaje flash inválido!. Tipo: "'. $tipo .'" / Mensaje: "'. $mensaje .'"');
        }
    }
}
