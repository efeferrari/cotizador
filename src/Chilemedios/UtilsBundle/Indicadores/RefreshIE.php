<?php

namespace Chilemedios\UtilsBundle\Indicadores;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RefreshIE
{
    private $container;
    
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    
    /**
     * Acutaliza el valor del dolar
     * @return boolean
     */
    public function f5()
    {
        # Intentar obtener data desde el banco central
        try
        {
            if($fd = @fopen('http://si3.bcentral.cl/indicadoresvalores/secure/indicadoresvalores.aspx', 'r'))
            {
                # ID del span que contiene el valor del dolar y U.F.
                $idDolar = 'RptListado_ctl03_lbl_valo';
                $idUF    = 'RptListado_ctl01_lbl_valo';

                while ($line = @fgets($fd, 1000))
                {
                    $regex = "/{$idDolar}/";
                    if(preg_match($regex, $line) )
                    {
                        $dolar = $this->limpiarValor($line);
                    }

                    $regex = "/{$idUF}/";
                    if(preg_match($regex, $line) )
                    {
                        $uf = $this->limpiarValor($line);
                    }
                }

                @fclose($fd);
                
            }
            else
            {
                throw new \Exception('No se pudo obtener los Indicadores Económicos desde BCentral!!', 10001);
            }
        }
        catch(\Exception $e)
        {
            $this->notificaError($e);
            return false;
        }
        
        try{
            $em = $this->container->get('doctrine')->getManager();

            # Sacar registros con los valores a actualizar
            $entityDolar = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find(1);
            $entityUF = $em->getRepository('ChilemediosCotizadorBundle:IndicadorEconomico')->find(2);

            # Actualizar valores
            $now = new \DateTime();

            $entityDolar->setValor( $dolar );
            $entityDolar->setLastUpdated( $now );

            $entityUF->setValor( $uf );
            $entityUF->setLastUpdated( $now );

            # Guardar los cambios
            $em->flush();
        }
        catch(\PDOException $e)
        {
            $this->notificaError($e);
            return false;
        }
        
        return true;
    }
    
    private function notificaError($e)
    {
        $msj = "Los indicadores económicos no fueron actualizados. El error capturado es el siguiente:<br><br>". $e->getMessage(). "
                <br><br>El código de error es {$e->getCode()}.<br><br>Archivo {$e->getFile()}.<br><br>Línea {$e->getLine()}";
                
        $message = \Swift_Message::newInstance()
            ->setSubject('[COTIZADOR '. date('Y-m-d H:i:s') .'] ERROR! No se actualizaron los Indicadores Económicos')
            ->setFrom('cotizaciones@chilemedios.cl')
            ->setTo('efe.ferrari@gmail.com')
            ->setBody(
                $msj,
                'text/html'
            )
        ;

        $this->container->get('mailer')->send($message);
    }
    
    /**
     * Formatea valores para guardar en base de datos. Eliminando los puntos
     * separadores y convierte las comas decimales en puntos. Además, saca otros
     * elementos no deseados, como espacios y símbolos.
     * @param string $valor
     * @return float 
     */
    private function limpiarValor($valor)
    {
        $sacar = array('$', '.',  ',', ' ');
        $poner = array('' , '' ,  '.', '');
        
        return (float) trim(strip_tags(str_replace($sacar, $poner, $valor)));
    }
}
