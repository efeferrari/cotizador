Cotizador de productos y servicios
==================================

Pequeña aplicación que permite hacer cotizaciones de productos.
Envía notificaciones por email y resúmenes en formato PDF.
Los productos pueden ser trabajados en dólares o UF, con datos actualizados 
desde el Banco Central. Además, las cotizaciones se guardan en la Base de Datos.

Cliente: Chilemedios
-------------------------------------------

Versión: 0.6 dev
-------------------------------------------

1) Requerimientos mínimos de la aplicación
-------------------------------------------

  * PHP 5.3.16 ([Recomendado por Symfony][1])
  * Módulo php mysql_pdo activo

2) Configuración del proyecto
-------------------------------

Antes de instalar la aplicación, se debe configurar la base de datos, la ubicación
del binario que genera los PDF y los datos para envío de correos. Esta info se 
puede revisar en el archivo "app/config/parameters.yml.dist".
El archivo binario para PDF está en la raíz del proyecto en versión 36 y 64 bits.


3) Instalando el proyecto
--------------------------

    * Instrucciones pendientes


4) Bundles dentro de la aplicación
------------------------------------

  * [Gregwar / CaptchaBundle][2]
  * [KNPBundle PDF][3]


Enjoy!

[1]:  http://symfony.com/doc/current/reference/requirements.html
[2]:  https://github.com/Gregwar/CaptchaBundle
[3]:  https://github.com/KnpLabs/KnpSnappyBundle
